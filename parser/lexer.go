package parser

import (
  "strings"
  "unicode"
)

type Token struct {
  name string
  text string
  lineIndex int
  runeIndex int
}

var validTokenNames map[string]string = map[string]string{
  "program" : "program",
  "gate" : "gate",
  "modifier" : "modifier",
  "literal" : "literal",
  "seperator" : "seperator",
  "operator" : "operator",
  //"eof" : "eof",
}

var validGateLexemes []string = []string{
  "import",
  "export",
  "extract",
  "model",
  "test",
}

func lex(code string) []Token {
  // split code into lines
  codeLines := strings.Split(code, "\n")

  tokens := []Token{}
  var tokenBuilder strings.Builder

  currLineIndex := 0
  currRuneIndex := 0

  indexGroupDepth := 0
  indexInWithParameters := false

  // iterate through runes on each line
  for currLineIndex <= len(codeLines) - 1 && currRuneIndex <= len(codeLines[currLineIndex]) - 1 {
    currLine := codeLines[currLineIndex]
    currLineRunes := []rune(currLine)
    currRune := currLineRunes[currRuneIndex]

    // handle runes delimiting identifiers and literals before spaces, equals, opening or closing curly braces, or opening or closing square brackets
    if (indexInWithParameters && currRune == '=') || ((unicode.IsSpace(currRune) || currRune == '{' || currRune == '}' || currRune == '[' || currRune == ']') && tokenBuilder.Len() > 0) {
      // handle gate declarations
      for _, validGateLexeme := range validGateLexemes {
        if tokenBuilder.String() == validGateLexeme {
          // append valid gate declaration and reset token builder
          tokens = append(tokens, Token{
            validTokenNames["gate"],
            tokenBuilder.String(),
            currLineIndex,
            currRuneIndex,
          })
          tokenBuilder.Reset()
        }
      }

      if tokenBuilder.String() == "from" || tokenBuilder.String() == "as" {
        // handle from/as
        tokens = append(tokens, Token{
          validTokenNames["modifier"],
          tokenBuilder.String(),
          currLineIndex,
          currRuneIndex,
        })
        tokenBuilder.Reset()
      } else if tokenBuilder.String() == "with" {
        // handle with
        tokens = append(tokens, Token{
          validTokenNames["modifier"],
          tokenBuilder.String(),
          currLineIndex,
          currRuneIndex,
        })
        tokenBuilder.Reset()

        indexInWithParameters = true
      } else if tokenBuilder.Len() > 0 {
        // handle other literals
        tokens = append(tokens, Token{
          validTokenNames["literal"],
          tokenBuilder.String(),
          currLineIndex,
          currRuneIndex,
        })
        tokenBuilder.Reset()
      }

      // handle with parameters
      if indexInWithParameters && currRune == '=' {
        tokens = append(tokens, Token{
          validTokenNames["operator"],
          "=",
          currLineIndex,
          currRuneIndex,
        })
      }

      currLineIndex, currRuneIndex = stepIndex(codeLines, currLineIndex, currRuneIndex)
      continue
    }

    // handle runes identifying curly braces
    if currRune == '{' || currRune == '}' {
      if currRune == '}' {
        indexInWithParameters = false
      }

      currLineIndex, currRuneIndex = stepIndex(codeLines, currLineIndex, currRuneIndex)
      continue
    }

    // handle runes identifying group opening seperator
    if currRune == '[' {
      // append opening group seperator and reset token builder
      tokens = append(tokens, Token{
        validTokenNames["seperator"],
        "[",
        currLineIndex,
        currRuneIndex,
      })
      tokenBuilder.Reset()

      indexGroupDepth++

      currLineIndex, currRuneIndex = stepIndex(codeLines, currLineIndex, currRuneIndex)
      continue
    }

    // handle runes delimiting group elements
    if indexGroupDepth > 0 {
      // handle closing element seperator
      if (currRune == ',' || currRune == ']') && tokenBuilder.Len() > 0 {
        tokens = append(tokens, Token{
          validTokenNames["literal"],
          tokenBuilder.String(),
          currLineIndex,
          currRuneIndex,
        })
        tokenBuilder.Reset()
      }

      // handle element seperator
      if currRune == ',' {
        currLineIndex, currRuneIndex = stepIndex(codeLines, currLineIndex, currRuneIndex)
        continue
      }

      // handle closing group seperator
      if currRune == ']' {
        // append closing group seperator and reset token builder
        tokens = append(tokens, Token{
          validTokenNames["seperator"],
          "]",
          currLineIndex,
          currRuneIndex,
        })
        tokenBuilder.Reset()

        indexGroupDepth--

        currLineIndex, currRuneIndex = stepIndex(codeLines, currLineIndex, currRuneIndex)
        continue
      }
    }

    // handle unknown rune
    if !unicode.IsSpace(currRune) {
      tokenBuilder.WriteRune(currRune)
    }

    currLineIndex, currRuneIndex = stepIndex(codeLines, currLineIndex, currRuneIndex)
  }

  return tokens
}

// step to next rune in lines of code
func stepIndex(codeLines []string, currLineIndex int, currRuneIndex int) (int, int) {
  if currRuneIndex >= len(codeLines[currLineIndex]) - 1 {
    return currLineIndex + 1, 0
  }
  return currLineIndex, currRuneIndex + 1
}

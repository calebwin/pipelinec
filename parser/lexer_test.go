package parser

import (
	"testing"
)

func TestLexerBasic(t *testing.T) {
  testCodeLines := []string{
    "import from {data.csv} as {my_data_records}",
    "extract from {my_data_records} as {   training_input_data} with  {[cols= [ 1,2]]}",
    "extract from {my_data_records} as {training_output_data}with {[cols = [3]]}",
    "model from{  [training_input_data, training_output_data]} as {my_model} with {[structure=perceptron]}",
    "test from {training_input_data} as {test_output_data}",
    "export from {  test_output_data} as {output.txt}",
  }
  testCode := assembleCode(testCodeLines)

	expectedTokenTexts := []string{"import", "from", "data.csv"}
	actualTokens := lex(testCode)

	for i, expectedTokenText := range expectedTokenTexts {
		if actualTokens[i].text != expectedTokenText {
			t.Error()
		}
	}
}

package parser

import (
	"testing"
  "strings"
	"fmt"
)

func TestParserBasic(t *testing.T) {
  testCodeLines := []string{
    "import from {data.csv} as {my_data_records}",
    "extract from {my_data_records} as {   training_input_data} with  {[cols= [ 1,2]]}",
    "extract from {my_data_records} as {training_output_data}with {[cols = [3]]}",
    "model from{  [training_input_data, training_output_data]} as {my_model} with {[structure=perceptron]}",
    "test from {training_input_data} as {test_output_data}",
    "export from {  test_output_data} as {output.txt}",
  }
  testCode := assembleCode(testCodeLines)

	codeAST := Parse(testCode)
	printAST(codeAST)

	if 1 + 2 != 3 {
		t.Error()
	}
}

func assembleCode(codeLines []string) string {
  return strings.Join(codeLines, "\n")
}

func printAST(inputASTNode ASTNode) {
	printASTFromLevel(inputASTNode, 0)
}

func printASTFromLevel(inputASTNode ASTNode, level int) {
	for i := 0; i < level - 1; i++ {
		fmt.Print(" ")
	}
	fmt.Print("|_")
	fmt.Println(inputASTNode.name + "," + strings.Replace(inputASTNode.value, " ", "_", -1))

	for _, inputASTNodeChild := range inputASTNode.children {
		printASTFromLevel(inputASTNodeChild, level + 1)
	}
}

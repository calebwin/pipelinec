package parser

import (
  "fmt"
)

var validTokenLeftContexts map[string][]string = map[string][]string{
  "gate" : []string{},
  "modifier" : []string{"gate"},
  "literal" : []string{},
  "seperator" : []string{},
  "operator" : []string{"literal"},
}

var validTokenRightContexts map[string][]string = map[string][]string{
  "gate" : []string{"modifier"},
  "modifier" : []string{},
  "literal" : []string{"operator"},
  "seperator" : []string{},
  "operator" : []string{},
}

type ASTNode struct {
  name string
  value string
  lineIndex int
  runeIndex int
  children []ASTNode
}

func Parse(code string) ASTNode {
  // tokenize code
  tokens := lex(code)

  // TODO validate token contexts
  for i, token := range tokens {
    validTokenLeftContext := validTokenLeftContexts[token.name]
    validTokenRightContext := validTokenRightContexts[token.name]

    // assert token's left context is correct
    for j := 0; j < len(validTokenLeftContext) - 1; j++ {
      if i - len(validTokenLeftContext) + j >= 0 && tokens[i - len(validTokenLeftContext) + j].text != validTokenLeftContext[j] {
        raiseSyntaxError(validTokenLeftContext[j] + " expected", token.lineIndex, token.runeIndex)
      }
    }

    // assert token's right context is correct
    for j := 0; j < len(validTokenRightContext) - 1; j++ {
      if i + 1 + j <= len(tokens) - 1 && tokens[i + 1 + j].text != validTokenRightContext[j] {
        raiseSyntaxError(validTokenRightContext[j] + " expected", token.lineIndex, token.runeIndex)
      }
    }
  }

  // generate AST nodes
  codeASTNodes := generateASTNodes(tokens)

  // generate AST
  codeAST := ASTNode{
    name : validTokenNames["program"],
    value : "",
    lineIndex : 1,
    runeIndex : 1,
    children : codeASTNodes,
  }

  return codeAST
}

// returns the first complete AST that can be obtained from the array of tokens
func generateASTNodes(tokenSlice []Token) []ASTNode {
  generatedASTNodes := []ASTNode{}

  // iterate through tokens
  currTokenIndex := 0
  for currTokenIndex <= len(tokenSlice) - 1 {
    currToken := tokenSlice[currTokenIndex]
    currLineIndex := currToken.lineIndex
    currRuneIndex := currToken.runeIndex

    // handle gates
    if currToken.name == "gate" {
      nextGateIndex := len(tokenSlice)

      // find end of gate code
      for i := currTokenIndex + 1; i <= len(tokenSlice) - 1; i++ {
        if tokenSlice[i].name == "gate" {
          nextGateIndex = i
          break
        }
      }

      // generate AST node from gate
      generatedASTNodes = append(generatedASTNodes, ASTNode{
        "gate",
        currToken.text,
        currLineIndex,
        currRuneIndex,
        generateASTNodes(tokenSlice[currTokenIndex + 1 : nextGateIndex]),
      })

      // update index
      currTokenIndex = nextGateIndex
    }

    // handle modifiers
    if currToken.name == "modifier" {
      endModifierValueIndex := len(tokenSlice)

      // find end of modifier value
      for i := currTokenIndex + 1; i <= len(tokenSlice) - 1; i++ {
        if tokenSlice[i].name == "modifier" || tokenSlice[i].name == "gate" {
          endModifierValueIndex = i
          break
        }
      }

      // generate AST node from modifier
      generatedASTNodes = append(generatedASTNodes, ASTNode{
        "modifier",
        currToken.text,
        currLineIndex,
        currRuneIndex,
        generateASTNodes(tokenSlice[currTokenIndex + 1 : endModifierValueIndex]),
      })

      // update index
      currTokenIndex = endModifierValueIndex
    }

    // handle group delimiters
    if currToken.name == "seperator" {
      // handle group prefix delimiter
      if currToken.text == "[" {
        groupEndIndex := len(tokenSlice)

        // find group end
        groupDepth := 1
        for i := currTokenIndex + 1; i <= len(tokenSlice) - 1; i++ {
          if tokenSlice[i].name == "seperator" {
            if tokenSlice[i].text == "[" {
              groupDepth++
            } else if tokenSlice[i].text == "]" {
              groupDepth--

              // check if group depth is same as it was at group start
              if groupDepth == 1 {
                groupEndIndex = i
                break
              }
            }
          }
        }

        // generate AST node as delimiter of first element in group's children
        firstElementPrefixDelimiterTokens := []Token{
          Token{
            "seperator",
            ",",
            currTokenIndex,
            currRuneIndex,
          },
        }

        // generate AST node from group
        generatedASTNodes = append(generatedASTNodes, ASTNode{
          "group",
          "",
          currLineIndex,
          currRuneIndex,
          generateASTNodes(append(firstElementPrefixDelimiterTokens, tokenSlice[currTokenIndex + 1 : groupEndIndex]...)),
        })

        // update index
        currTokenIndex = groupEndIndex + 1
      }

      // handle group element prefix delimiter
      if currToken.text == "," {
        nextSeperatorIndex := len(tokenSlice)

        // find next seperator
        groupDepth := 0
        for i := currTokenIndex + 1; i <= len(tokenSlice) - 1; i++ {
          // adjust group depth
          if tokenSlice[i].name == "seperator" {
            if tokenSlice[i].text == "[" {
              groupDepth++
            } else if tokenSlice[i].text == "]" {
              groupDepth--
            }
          }

          // check if current token is next seperator
          if groupDepth == 0 && tokenSlice[i].name == "seperator" {
            if tokenSlice[i].text == "," || tokenSlice[i].text == "]" {
              nextSeperatorIndex = i
              break
            }
          }
        }

        // generate AST node from group element
        generatedASTNodes = append(generatedASTNodes, generateASTNodes(tokenSlice[currTokenIndex + 1 : nextSeperatorIndex])...)

        // update index
        if nextSeperatorIndex <= len(tokenSlice) - 1 && tokenSlice[nextSeperatorIndex].text == "," {
          currTokenIndex = nextSeperatorIndex
        } else {
          currTokenIndex = nextSeperatorIndex + 1
        }
      }
    }

    // handle literals
    if currToken.name == "literal" {
      // handle parameter name
      if currTokenIndex + 1 <= len(tokenSlice) - 1 && tokenSlice[currTokenIndex + 1].name == "operator" && tokenSlice[currTokenIndex + 1].text == "=" {
        parameterValueEndIndex := len(tokenSlice)

        // find end of parameter values
        groupDepth := 0
        for i := currTokenIndex + 1; i <= len(tokenSlice) - 1; i++ {
          // adjust group depth
          if tokenSlice[i].name == "seperator" {
            if tokenSlice[i].text == "[" {
              groupDepth++
            }
            if tokenSlice[i].text == "]" {
              groupDepth--
            }

            if groupDepth <= 0 {
              parameterValueEndIndex = i
              break
            }
          }
        }

        // generate AST node for parameter
        generatedASTNodes = append(generatedASTNodes, ASTNode{
          "parameter",
          currToken.text,
          currLineIndex,
          currRuneIndex,
          generateASTNodes(tokenSlice[currTokenIndex + 1 : parameterValueEndIndex]),
        })

        // update index
        if parameterValueEndIndex <= len(tokenSlice) - 1 && tokenSlice[parameterValueEndIndex].text == "," {
          currTokenIndex = parameterValueEndIndex
        } else {
          currTokenIndex = parameterValueEndIndex + 1
        }
      } else {
        // generate AST node for literal
        generatedASTNodes = append(generatedASTNodes, ASTNode{
          "literal",
          currToken.text,
          currLineIndex,
          currRuneIndex,
          []ASTNode{},
        })

        // update index
        currTokenIndex = currTokenIndex + 1
      }
    }

    // handle operators
    if currToken.name == "operator" {
      // update index
      currTokenIndex = currTokenIndex + 1
    }
  }

  return generatedASTNodes
}

func raiseSyntaxError(errorMessage string, lineIndex int, runeIndex int) {
  fmt.Println("syntax error: " + errorMessage + " at " + string(lineIndex + 1) + ":" + string(runeIndex + 1))
}

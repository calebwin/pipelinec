package main

import (
  "os"
  //"fmt"
  "path/filepath"
  "io"
  "io/ioutil"
  "strings"
  "github.com/calebwin/pipelinec/parser"
  "github.com/calebwin/pipelinec/generator"
)

func main() {
  // load file
  relativeFilePath := os.Args[1]
  absoluteFilePath, _ := filepath.Abs(relativeFilePath)
  codeFileContents := readStringFromFile(absoluteFilePath)

  // parse file
  codeAST := parser.Parse(codeFileContents)

  // generate bytecode
  compiledCode := generator.GenerateCode(codeAST, strings.Replace(relativeFilePath, ".pipeline", "", -1))

  // print bytecode to new file
  newAbsoluteFilePath, _ := filepath.Abs(relativeFilePath)
  writeStringToFile(newAbsoluteFilePath, compiledCode)
}

func readStringFromFile(absoluteFilePath string) string {
  codeFile, err := ioutil.ReadFile(absoluteFilePath)
  if err != nil {
      return ""
  }

  return string(codeFile)
}

// writes given string to file at given absolute path
// creates new file if file does not exist
func writeStringToFile(absoluteFilePath string, str string) error {
  // create new file
	newFile, err := os.Create(absoluteFilePath)
	if err != nil {
		return err
	}
	defer newFile.Close()

  // write to file
	_, err = io.Copy(newFile, strings.NewReader(str))
	if err != nil {
		return err
	}

	return nil
}
